import { Component, OnInit } from '@angular/core';
import { Entity } from '../shared/software-elements/entity';
import { UseCase } from '../shared/software-elements/use-case';
import { Presenter } from '../shared/software-elements/presenter';
import { View } from '../shared/software-elements/view';
import { ArchService } from '../io/arch.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { EntitiesComponent } from '../entities/entities.component';
import { SoftwareElementHolder } from '../shared/software-elements-holders/software-element-holder';
import { EntityHolder } from '../shared/software-elements-holders/entity-holder';
import { SoftwareElementAction } from '../shared/software-elements-holders/software-element-action';
import { NewFlowComponent } from '../new-flow/new-flow.component';
import { NewFlowInputData } from '../shared/software-elements-holders/new-flow-input';
import { DefaultEntitiesService } from '../io/default-entities.ervice';
import { Flow } from '../shared/software-elements-holders/flow';
import { DiagramsComponent } from '../diagrams/diagrams.component';
import { DummydataService } from '../io/dummy-data-service';
import { XMLBuilderService } from '../io/xml-builder-service';

@Component({
  selector: 'app-software-layers',
  templateUrl: './software-layers.component.html',
  styleUrls: ['./software-layers.component.css']
})
export class SoftwareLayersComponent implements OnInit {

  panelOpenState = false;
  public entities: Entity[] = [];
  public useCases: UseCase[] = [];
  public presenters: Presenter[] = [];
  public views: View[] = [];
  flows: Flow[] = [];

  constructor(private archService: ArchService, private matDialog: MatDialog,
    private defaultEntitiesService: DefaultEntitiesService,
    private dummyDataService: DummydataService,
    private xMLBuilderService: XMLBuilderService) {

    dummyDataService.getDummyEntities().forEach(e => { this.entities.push(e); });
    dummyDataService.getDummyUseCases().forEach(u => { this.useCases.push(u); });
    dummyDataService.getDummyPresenters().forEach(p => { this.presenters.push(p); });
    dummyDataService.getDummyViews().forEach(v => { this.views.push(v); });

    this.flows.push(new Flow("login", [...this.useCases], [...this.presenters], [...this.views]));
  }

  ngOnInit(): void {
  }

  download() {
    const xml = this.xMLBuilderService.createXML(this.entities, this.useCases, this.presenters, this.views);
    console.log(xml);

    this.archService.doTheParsing(xml);
  }

  downloadXML(): void {
    const xml = this.xMLBuilderService.createXML(this.entities, this.useCases, this.presenters, this.views);

    var ab = new ArrayBuffer(xml.length);
    var ia = new Uint8Array(ab);

    for (var i = 0; i < xml.length; i++) {
      ia[i] = xml.charCodeAt(i);
    }
    let blob: Blob = new Blob([ab], { type: 'xml' });

    var url = window.URL.createObjectURL(blob);

    var a: any = document.createElement("a");
    a.href = url;
    a.download = "structure.xml";
    a.click();
    window.URL.revokeObjectURL(url);
  }

  onEntitySelected(entity: Entity) {
    let entityHolder = new EntityHolder(entity, entity.typePackage, SoftwareElementAction.UPDATE);
    this.openModal(entityHolder);
  }

  onFlowSelected(flow: Flow) {
    this.openDiagramsModal(flow);
  }

  onNewEntityClicked() {
    let entityHolder = new EntityHolder(null, null, SoftwareElementAction.CREATE);
    this.openModal(entityHolder);
  }

  onNewFlowClicked() {
    this.openNewFlowModal();
  }

  openNewFlowModal() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "90%";
    dialogConfig.width = "75%";
    dialogConfig.autoFocus = false;

    let entitiesToSend = [...this.entities];
    let defaultEntities = this.defaultEntitiesService.getDefaultentities();
    defaultEntities.forEach(val => entitiesToSend.push(Object.assign({}, val)));

    dialogConfig.data = new NewFlowInputData(entitiesToSend, this.useCases, this.presenters, this.views);

    const modalDialog = this.matDialog.open(NewFlowComponent, dialogConfig);

    modalDialog.componentInstance.flowEmitter.subscribe((receivedEntry: Flow) => {
      this.flows.push(receivedEntry);

      //extract newly created use cases
      receivedEntry.usecases.forEach(usecase => {
        let index = this.indexOf(this.useCases, (u: UseCase) => {
          return usecase.name === u.name;
        });

        if (index === -1) {
          this.useCases.push(usecase);
        }

      });

      //extract newly created presenters
      receivedEntry.presenters.forEach(presenter => {
        let index = this.indexOf(this.presenters, (p: Presenter) => {
          return presenter.name === p.name;
        });

        if (index === -1) {
          this.presenters.push(presenter);
        }

      });

      //extract newly created views
      receivedEntry.views.forEach(view => {
        let index = this.indexOf(this.views, (v: View) => {
          return view.name === v.name;
        });

        if (index === -1) {
          this.views.push(view);
        }

      });

      modalDialog.close();
    });
  }

  openDiagramsModal(flow: Flow) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "90%";
    dialogConfig.width = "75%";
    dialogConfig.autoFocus = true;
    dialogConfig.data = flow;

    const modalDialog = this.matDialog.open(DiagramsComponent, dialogConfig);
  }

  openModal(softwareElemHolder: SoftwareElementHolder) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "90%";
    dialogConfig.width = "75%";
    softwareElemHolder.allEntities = this.entities;

    dialogConfig.data = softwareElemHolder;
    dialogConfig.autoFocus = false;

    const modalDialog = this.matDialog.open(EntitiesComponent, dialogConfig);

    modalDialog.componentInstance.entityEmitter.subscribe((receivedEntry) => {
      switch (receivedEntry.action) {
        case SoftwareElementAction.CREATE:
          this.entities.push(receivedEntry);
          break;
        case SoftwareElementAction.UPDATE:
          let index = this.indexOf(this.entities, (e: Entity) => {
            return e.name === receivedEntry.name;
          });

          if (index === -1) {
            index = this.entities.length;
          }

          this.entities[index] = { ...receivedEntry };

          break;
      }
      modalDialog.close();
    });
  }

  indexOf<T>(arr: T[], criteria: (ent: T) => boolean): number {

    for (let i = 0; i < arr.length; i++) {
      let arrElem = arr[i];
      if (criteria(arrElem)) {
        return i;
      }
    }

    return -1;
  }
}
