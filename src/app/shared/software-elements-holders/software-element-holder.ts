import { Method } from '../software-element-parts/method';
import { Field } from '../software-element-parts/field';
import { Entity } from '../software-elements/entity';
import { SoftwareElementAction } from './software-element-action';
import { Layer } from '../software-element-parts/layer';
import { UseCase } from '../software-elements/use-case';

export abstract class SoftwareElementHolder {
    allEntities: Entity[];
    allUseCases: UseCase[];
    mainPackageName: string;
    softwareElementAction: SoftwareElementAction

    constructor(mainPackageName: string, softwareElementAction: SoftwareElementAction) {
        this.mainPackageName = mainPackageName;
        this.softwareElementAction = softwareElementAction
    }

    abstract getName(): string;
    abstract getFields(): Field[];
    abstract getMethods(): Method[];
    abstract allowMethods(): boolean;
    abstract getLayer(): Layer
}