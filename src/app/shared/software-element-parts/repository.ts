import { Method } from './method';
import { Listener } from './listener';

export class Repository {
    name: string;
    methods: Method[];
    listeners: Listener[];

    constructor(name: string, methods: Method[], listeners: Listener[]) {

        this.name = name;
        this.methods = methods;
        this.listeners = listeners;
    }
}
