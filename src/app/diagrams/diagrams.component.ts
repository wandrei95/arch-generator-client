import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit, Inject } from '@angular/core';
import * as mermaid from "mermaid";
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Flow } from 'src/app/shared/software-elements-holders/flow';
import { ViewportScroller } from '@angular/common';
import { Field } from 'src/app/shared/software-element-parts/field';
import { Repository } from 'src/app/shared/software-element-parts/repository';
import { View } from 'src/app/shared/software-elements/view';
import { Presenter } from 'src/app/shared/software-elements/presenter';
import { UseCase } from 'src/app/shared/software-elements/use-case';
import { Method } from 'src/app/shared/software-element-parts/method';

@Component({
  selector: 'app-diagrams',
  templateUrl: './diagrams.component.html',
  styleUrls: ['./diagrams.component.css']
})
export class DiagramsComponent implements AfterViewInit {

  constructor(@Inject(MAT_DIALOG_DATA) private flow: Flow) {
  }

  @ViewChild("classDiagramDiv")
  public classDiagramDiv;
  @ViewChild("sequenceDiagramDiv")
  public sequenceDiagramDiv;

  public ngAfterViewInit(): void {
    mermaid.initialize({
      theme: "dark"
    });

    this.drawClassDiagram();

    this.drawSequenceDiagram();
  }


  private drawClassDiagram() {
    const element: any = this.classDiagramDiv.nativeElement;

    let graphDefinition: String = `classDiagram`;

    graphDefinition = this.addViewsToClassDiagram(graphDefinition);

    graphDefinition = this.addPresentersToClassDiagram(graphDefinition);

    graphDefinition = this.addUseCasesToClassDiagram(graphDefinition);

    mermaid.render("classDiagram", graphDefinition, (svgCode, bindFunctions) => {
      element.innerHTML = svgCode;
    });
  }

  private addViewsToClassDiagram(graphDefinition: String): String {
    //link between the view and the interfaces it implements
    this.flow.views.forEach(view => {
      view.interfaces.forEach(viewInt => {
        graphDefinition += `\n ${view.name} ..|> ${viewInt.name}`;
      });

      //link between the view and the presenters that it uses
      view.fields.forEach(field => {
        if (this.isFieldPresenter(field)) {
          graphDefinition += `\n ${view.name} o-- ${field.type}`;
        }
      });

      //add attributes to the view
      graphDefinition += `\n class ${view.name}{`;

      view.fields.forEach(field => {
        graphDefinition += `\n ${field.type} ${field.name}`;
      });
      view.methods.forEach(mth => {
        graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
      });

      view.interfaces.forEach(i => {
        i.methods.forEach(mth => {
          graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
        });
      });

      graphDefinition += `\n}`;

    });

    return graphDefinition;
  }

  private addPresentersToClassDiagram(graphDefinition: String): String {
    this.flow.presenters.forEach(presenter => {
      //link between the presenter and the listeners defined inside of it
      presenter.listeners.forEach(presListener => {
        graphDefinition += `\n ${presenter.name} o-- ${presListener.name}`;
      });

      //link between the presenter and the use cases that it uses
      presenter.fields.forEach(field => {
        if (this.isFieldUseCase(field)) {
          graphDefinition += `\n ${presenter.name} o-- ${field.type}`;
        }
      });

      //link between the presenter and the use case listeners
      presenter.methods.forEach(mth => {
        let callbackInfo = mth.callbackInfo;
        if (callbackInfo) {
          let callBackClass = callbackInfo.callBackClass;
          if (callBackClass) {
            graphDefinition += `\n ${callBackClass} <-- ${presenter.name}`;
          }
        }
      });

      //add attributes to the presenter
      graphDefinition += `\n class ${presenter.name}{`;

      presenter.fields.forEach(field => {
        graphDefinition += `\n ${field.type} ${field.name}`;
      });

      if (presenter.bindingList) {
        let bl = presenter.bindingList;
        graphDefinition += `\n List~${bl.parametrizedType}~ ${bl.name}`;
      }

      presenter.methods.forEach(mth => {
        graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
      });
      graphDefinition += `\n}`;

      presenter.listeners.forEach(presListener => {
        graphDefinition += `\n class ${presListener.name}{`;
        graphDefinition += `<<interface>>`;
        presListener.methods.forEach(mth => {
          graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
        });
        graphDefinition += `\n}`;
      });

    });

    return graphDefinition;
  }

  private addUseCasesToClassDiagram(graphDefinition: String): String {
    this.flow.usecases.forEach(useCase => {
      //link between the use case and the listeners defined inside of it
      useCase.listeners.forEach(useCaseListener => {
        graphDefinition += `\n ${useCaseListener.name} <-- ${useCase.name}`;
      });

      //link between the presenter and the repositoeis that it uses
      useCase.fields.forEach(field => {
        if (this.isFieldRepository(field)) {
          graphDefinition += `\n ${useCase.name} o-- ${field.type}`;
        }
      });

      //link between the use case and the repository listeners
      useCase.methods.forEach(mth => {
        let callbackInfo = mth.callbackInfo;
        if (callbackInfo) {
          let callBackClass = callbackInfo.callBackClass;
          if (callBackClass) {
            graphDefinition += `\n ${callBackClass} <-- ${useCase.name}`;
          }
        }
      });

      //link between each repository and it's listener
      useCase.repositories.forEach(repo => {
        repo.listeners.forEach(repoListener => {
          graphDefinition += `\n ${repoListener.name} <-- ${repo.name}`;
        });
      });

      //add attributes to the use case
      graphDefinition += `\n class ${useCase.name}{`;

      useCase.fields.forEach(field => {
        graphDefinition += `\n ${field.type} ${field.name}`;
      });
      useCase.methods.forEach(mth => {
        graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
      });
      graphDefinition += `\n}`;

      useCase.listeners.forEach(useCaseListener => {
        graphDefinition += `\n class ${useCaseListener.name}{`;
        graphDefinition += `<<interface>>`;
        useCaseListener.methods.forEach(mth => {
          graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
        });
        graphDefinition += `\n}`;
      });

      useCase.repositories.forEach(repo => {
        graphDefinition += `\n class ${repo.name}{`;
        graphDefinition += `<<interface>>`;
        repo.methods.forEach(mth => {
          graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
        });
        graphDefinition += `\n}`;

        repo.listeners.forEach(repoListener => {
          graphDefinition += `\n class ${repoListener.name}{`;
          graphDefinition += `<<interface>>`;
          repoListener.methods.forEach(mth => {
            graphDefinition += `\n ${mth.name}(${this.buildListOfParams(mth)})`;
          });
          graphDefinition += `\n}`;
        });
      });


    });

    return graphDefinition;
  }

  private buildListOfParams(mth: Method): string {
    let listOfParams = "";
    for (let i = 0; i < mth.parameters.length; i++) {
      let param = mth.parameters[i];
      listOfParams += `${param.type} ${param.name}`;
      if (i !== mth.parameters.length - 1) {
        listOfParams += ", "
      }
    }
    return listOfParams;
  }

  private drawSequenceDiagram() {
    const element: any = this.sequenceDiagramDiv.nativeElement;

    for (let i = 0; i < this.flow.views.length; i++) {
      let view = this.flow.views[i];

      let graphDefinition: String = `sequenceDiagram`;

      graphDefinition = this.createSequenceDiagramforView(view, graphDefinition);

      mermaid.render("sequenceDiagram" + i, graphDefinition, (svgCode, bindFunctions) => {
        element.innerHTML = svgCode;
      });
    }
  }

  private isFieldPresenter(field: Field): boolean {
    for (let i = 0; i < this.flow.presenters.length; i++) {
      let pres = this.flow.presenters[i];
      if (pres.name === field.type) {
        return true;
      }
    }

    return false;
  }

  private isFieldUseCase(field: Field): boolean {
    for (let i = 0; i < this.flow.usecases.length; i++) {
      let useCase = this.flow.usecases[i];
      if (useCase.name === field.type) {
        return true;
      }
    }

    return false;
  }

  private isFieldRepository(field: Field): boolean {
    for (let i = 0; i < this.flow.usecases.length; i++) {
      let useCase = this.flow.usecases[i];
      let repositories = useCase.repositories;

      for (let j = 0; j < repositories.length; j++) {
        let repo = repositories[j];
        if (repo.name === field.type) {
          return true;
        }
      }
    }

    return false;
  }

  private createSequenceDiagramforView(view: View, graphDefinition: String): String {
    graphDefinition += `hereAddRepoImpl`;
    view.methods.forEach(mth => {
      let callCode = mth.callCode;
      if (callCode) {
        let objectToCall = callCode.objectToCall;
        let methodToCall = callCode.methodToCall;

        let presenter = this.getPresenterForName(this.capitalize(objectToCall));

        if (presenter) {
          let presenterMethod = this.getMethodForName(presenter.methods, methodToCall);
          if (presenterMethod) {
            let presMthcallBackInfo = presenterMethod.callbackInfo;
            if (presMthcallBackInfo) {
              let cbName = presMthcallBackInfo.callBackName;
              let cbMethodName = presMthcallBackInfo.callBackMethodName;
              let successUseCaseCallBack = presMthcallBackInfo.callbacks[0].name;
              let failureUseCaseCallBack = presMthcallBackInfo.callbacks[1].name;
              let useCase = this.getUseCaseForName(this.capitalize(cbName));
              if (useCase) {
                let useCaseMethod = this.getMethodForName(useCase.methods, cbMethodName);
                if (useCaseMethod) {
                  let useCaseMthcallBackInfo = useCaseMethod.callbackInfo;
                  if (useCaseMthcallBackInfo) {
                    let useCaseCallBackName = useCaseMthcallBackInfo.callBackName;
                    let useCaseCallBackMethodName = useCaseMthcallBackInfo.callBackMethodName;
                    let successRepoCallBack = useCaseMthcallBackInfo.callbacks[0].name;
                    let failureRepoCallBack = useCaseMthcallBackInfo.callbacks[1].name;

                    let repositoryName = this.capitalize(useCaseCallBackName);

                    if (graphDefinition.includes(`hereAddRepoImpl`)) {
                      graphDefinition = graphDefinition.replace(`hereAddRepoImpl`, `hereAddRepoImpl\n participant ${repositoryName}Impl`);
                    }
                    // graphDefinition += `\n participant ${repositoryName}Impl`
                    graphDefinition += `\n participant ${view.name}`
                    graphDefinition += `\n participant ${presenter.name}`
                    graphDefinition += `\n participant ${useCase.name}`

                    graphDefinition += `\n ${view.name}->>${presenter.name}: ${presenterMethod.name}`;
                    graphDefinition += `\n ${presenter.name}->>${useCase.name}: ${useCaseMethod.name}`;
                    graphDefinition += `\n ${useCase.name}->>${repositoryName}Impl: ${useCaseCallBackMethodName}`;

                    graphDefinition += `\n ${repositoryName}Impl->>${useCase.name}: ${successRepoCallBack}/${failureRepoCallBack}`;
                    graphDefinition += `\n ${useCase.name}->>${presenter.name}:${successUseCaseCallBack}/${failureUseCaseCallBack}`;

                    let successPresenterCallback = presMthcallBackInfo.callbacks[0].listenerMethodName;
                    let failurePresenterCallback = presMthcallBackInfo.callbacks[1].listenerMethodName;

                    graphDefinition += `\n ${presenter.name}->>${view.name}: ${successPresenterCallback}/${failurePresenterCallback}`;

                  }
                }
              }
            }
          }
        }
      }
    });

    graphDefinition = graphDefinition.replace(`hereAddRepoImpl`, ``);

    return graphDefinition;
  }

  private getPresenterForName(name: String): Presenter {
    for (let i = 0; i < this.flow.presenters.length; i++) {
      let pres = this.flow.presenters[i];
      if (name === pres.name) {
        return pres;
      }
    }

    return null;
  }

  private getMethodForName(methods: Method[], name: String): Method {
    for (let i = 0; i < methods.length; i++) {
      let mth = methods[i];
      if (name === mth.name) {
        return mth;
      }
    }

    return null;
  }

  private getUseCaseForName(name: String): UseCase {
    for (let i = 0; i < this.flow.usecases.length; i++) {
      let useCase = this.flow.usecases[i];
      if (name === useCase.name) {
        return useCase;
      }
    }

    return null;
  }

  private capitalize(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  private deCapitalize(text: string): string {
    return text.charAt(0).toLowerCase() + text.slice(1);
  }
}
