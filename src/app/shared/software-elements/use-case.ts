import { Entity } from './entity';
import { Method } from '../software-element-parts/method';
import { Field } from '../software-element-parts/field';
import { Listener } from '../software-element-parts/listener';
import { Repository } from '../software-element-parts/repository';
import { SoftwareElementAction } from '../software-elements-holders/software-element-action';

export class UseCase extends Entity {
    methods: Method[];
    listeners: Listener[];
    repositories: Repository[];
    action: SoftwareElementAction;

    constructor(name: string, typePackage: string, fields: Field[], methods: Method[],
        listeners: Listener[], repositories: Repository[]) {

        super(name, typePackage, fields);
        this.methods = methods;
        this.listeners = listeners;
        this.repositories = repositories;
    }
}