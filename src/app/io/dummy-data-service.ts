import { Injectable } from '@angular/core';
import { Entity } from '../shared/software-elements/entity';
import { Field } from '../shared/software-element-parts/field';
import { Method } from '../shared/software-element-parts/method';
import { Callback } from '../shared/software-element-parts/callback';
import { CallBackInfo } from '../shared/software-element-parts/callback-info';
import { Listener } from '../shared/software-element-parts/listener';
import { Repository } from '../shared/software-element-parts/repository';
import { UseCase } from '../shared/software-elements/use-case';
import { BindingList } from '../shared/software-element-parts/binding-list';
import { Presenter } from '../shared/software-elements/presenter';
import { CallCode } from '../shared/software-element-parts/call-code';
import { View } from '../shared/software-elements/view';

@Injectable({
    providedIn: 'root'
})
export class DummydataService {

    private entities = [];
    private useCases = [];
    private presenters = [];
    private views = [];

    constructor() {
        this.buildEntities();
        this.buildUseCases();
        this.buildPresenters();
        this.buildViews();
    }

    public getDummyEntities(): Entity[] {
        return this.entities;
    }

    public getDummyUseCases(): UseCase[] {
        return this.useCases;
    }

    public getDummyPresenters(): Presenter[] {
        return this.presenters;
    }

    public getDummyViews(): View[] {
        return this.views;
    }

    private buildEntities() {
        let idField = new Field("id", "String", "", true);
        let emailField = new Field("email", "String", "", true);
        let passwordField = new Field("password", "String", "", true);
        let firstNameField = new Field("firstName", "String", "", true);
        let lastNameField = new Field("lastName", "String", "", true);
        let useEntity = new Entity("User", "com.andrei.wegroszta.codegen.entities", [idField, emailField, passwordField, firstNameField, lastNameField]);
        this.entities.push(useEntity);

        // let ownerField = new Field("owner", "User", "com.andrei.wegroszta.codegen.entities", true);
        // let nameField = new Field("name", "String", "", true);
        // let carEntity = new Entity("Car", "com.andrei.wegroszta.codegen.entities", [idField, nameField, ownerField]);
        // this.entities.push(carEntity);
    }

    private buildUseCases() {
        let userRepositoryField = new Field("loginRepository", "LoginRepository", "com.andrei.wegroszta.codegen.usecases.login", true)

        let userParam = new Field("user", "User", "com.andrei.wegroszta.codegen.entities", false);
        let exParam = new Field("ex", "Throwable", "", false);

        let emailParam = new Field("email", "String", "", false);
        let passwordParam = new Field("password", "String", "", false);
        let loginListenerParam = new Field("loginListener", "LoginListener", "com.andrei.wegroszta.codegen.usecases.login.Login", false);
        let loginMethod = new Method("login", [emailParam, passwordParam, loginListenerParam], false);
        let onUserFoundCallback = new Callback("onLoginSuccess", "loginListener", "onLoginSuccess", [userParam]);
        let onFailureCallback = new Callback("onLoginFailure", "loginListener", "onLoginFailure", [exParam]);
        let loginCallbackInfo = new CallBackInfo("loginRepository", "login", "LoginRepositoryListener", "com.andrei.wegroszta.codegen.usecases.login.LoginRepository",
            [emailParam, passwordParam], [onUserFoundCallback, onFailureCallback]);
        loginMethod.callbackInfo = loginCallbackInfo;

        let onUserLoggedInMethod = new Method("onLoginSuccess", [userParam], true);

        let errorParam = new Field("ex", "Throwable", "", false);
        let onUserLoginFailMethod = new Method("onLoginFailure", [errorParam], true);

        let loginListener = new Listener("LoginListener", "", [onUserLoggedInMethod, onUserLoginFailMethod]);

        let findUserListenerParam = new Field("listener", "LoginRepositoryListener", "com.andrei.wegroszta.codegen.usecases.login.LoginRepository", false);
        let loginRepoMethod = new Method("login", [emailParam, passwordParam, findUserListenerParam], true);
        let onUserFoundMethod = new Method("onLoginSuccess", [userParam], true);

        let onFailureMethod = new Method("onLoginFailure", [exParam], true);
        let findUserListener = new Listener("LoginRepositoryListener", "", [onUserFoundMethod, onFailureMethod]);

        let userRepository = new Repository("LoginRepository", [loginRepoMethod], [findUserListener]);

        let loginUserUseCase = new UseCase("Login", "com.andrei.wegroszta.codegen.usecases.login", [userRepositoryField],
            [loginMethod], [loginListener], [userRepository]);

        this.useCases.push(loginUserUseCase);
    }

    private buildPresenters() {
        let userParam = new Field("user", "User", "com.andrei.wegroszta.codegen.entities", false);
        let exParam = new Field("ex", "Throwable", "", false);

        let loginUserField = new Field("login", "Login", "com.andrei.wegroszta.codegen.usecases.login", true);

        let loginViewsBindingList = new BindingList("loginViews", "LoginView", "com.andrei.wegroszta.codegen.presenters.login.LoginPresenter");

        let emailParam = new Field("email", "String", "", false);
        let passwordParam = new Field("password", "String", "", false);

        let secondLoginMethod = new Method("authenticate", [emailParam, passwordParam], false);
        let secondOnUserLoggedInCallback = new Callback("onLoginSuccess", "loginView", "onAuthenticateSuccess", [userParam]);
        let secondOnUserLoginFailCallback = new Callback("onLoginFailure", "loginView", "onAuthenticateFailure", [exParam]);
        let loginBindingList = new BindingList("loginViews", "LoginView", "com.andrei.wegroszta.codegen.presenters.login.LoginPresenter");
        secondOnUserLoggedInCallback.bindingList = loginBindingList;
        secondOnUserLoginFailCallback.bindingList = loginBindingList;
        let secondLoginCallbackInfo = new CallBackInfo("login", "login", "LoginListener", "com.andrei.wegroszta.codegen.usecases.login.Login",
            [emailParam, passwordParam], [secondOnUserLoggedInCallback, secondOnUserLoginFailCallback]);
        secondLoginMethod.callbackInfo = secondLoginCallbackInfo;

        let onLogInMethod = new Method("onAuthenticateSuccess", [userParam], true);
        let errorParam = new Field("ex", "Throwable", "", false);
        let onLoginFailMethod = new Method("onAuthenticateFailure", [errorParam], true);
        let loginViewListener = new Listener("LoginView", "", [onLogInMethod, onLoginFailMethod]);

        let loginPresenter = new Presenter("LoginPresenter", "com.andrei.wegroszta.codegen.presenters.login",
            [loginUserField], loginViewsBindingList, [secondLoginMethod], [loginViewListener]);

        this.presenters.push(loginPresenter);
    }

    private buildViews() {
        let loginPresenterField = new Field("loginPresenter", "LoginPresenter", "com.andrei.wegroszta.codegen.presenters.login", true);

        let userParam = new Field("user", "User", "com.andrei.wegroszta.codegen.entities", false);
        let onLogInMethod = new Method("onAuthenticateSuccess", [userParam], false);
        onLogInMethod.override = true;
        let errorParam = new Field("ex", "Throwable", "", false);
        let onLoginFailMethod = new Method("onAuthenticateFailure", [errorParam], false);
        onLoginFailMethod.override = true;
        let loginViewInterface = new Listener("LoginView", "com.andrei.wegroszta.codegen.presenters.login.LoginPresenter", [onLogInMethod, onLoginFailMethod]);

        let emailParam = new Field("email", "String", "", false);
        let passwordParam = new Field("password", "String", "", false);
        let loginMethod = new Method("login", [emailParam, passwordParam], false);
        let loginMethodCallCode = new CallCode("loginPresenter", "authenticate", ["email", "password"]);
        loginMethod.callCode = loginMethodCallCode;

        let loginView = new View("SomeLoginView", "com.andrei.wegroszta.codegen.views.login", [loginPresenterField],
            [loginMethod], [loginViewInterface]);

        this.views.push(loginView);
    }
}
