import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { Field } from 'src/app/shared/software-element-parts/field';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Entity } from 'src/app/shared/software-elements/entity';
import { Method } from 'src/app/shared/software-element-parts/method';
import { NewFlowInputData } from 'src/app/shared/software-elements-holders/new-flow-input';
import { UseCase } from 'src/app/shared/software-elements/use-case';
import { Presenter } from 'src/app/shared/software-elements/presenter';
import { View } from 'src/app/shared/software-elements/view';
import { Callback } from 'src/app/shared/software-element-parts/callback';
import { Listener } from 'src/app/shared/software-element-parts/listener';
import { Repository } from 'src/app/shared/software-element-parts/repository';
import { CallBackInfo } from 'src/app/shared/software-element-parts/callback-info';
import { BindingList } from 'src/app/shared/software-element-parts/binding-list';
import { Interface } from 'src/app/shared/software-element-parts/interface';
import { CallCode } from 'src/app/shared/software-element-parts/call-code';
import { Flow } from 'src/app/shared/software-elements-holders/flow';
import { ParameterizedType } from '../shared/software-element-parts/type';
import { VirtualTimeScheduler } from 'rxjs';

interface NewFlow {
  mainPackageName: string;
  flowName: string;
}

interface NewField {
  entity: Entity;
  parameterizedEntity: Entity;
  fieldName: string;
}

interface NewUseCaseMethod {
  mehtodName: string;
  parameters: Field[];
  returnType: Entity;
  returnParameterizedEntity: Entity;
}

interface NewPresenterMethod {
  mehtodName: string;
  parameters: Field[];
  returnType: Entity;
  returnParameterizedEntity: Entity;
  useCaseToCall: UseCase;
  useCaseMethodToCall: Method;
}

interface NewViewMethod {
  mehtodName: string;
  parameters: Field[];
  returnType: Entity;
  returnParameterizedEntity: Entity;
  presenterToCall: UseCase;
  presenterMethodToCall: Method;
}

interface NewUseCaseToFlow {
  useCase: UseCase;
}

interface NewPresenterToFlow {
  presenter: Presenter;
}

@Component({
  selector: 'app-new-flow',
  templateUrl: './new-flow.component.html',
  styleUrls: ['./new-flow.component.css']
})
export class NewFlowComponent implements OnInit {

  mainPackageName: string;
  flowName: string

  isNewFlowTab: boolean = true;
  isUseCaseTab: boolean = false;
  isPresenterTab: boolean = false;
  isViewTab: boolean = false;
  isTypedParameterOpened: boolean = false;
  isReturnTypedParameterOpened: boolean = false;
  isMethodParamTypedParameterOpened: boolean = false;

  allEntities: Entity[];
  nonPrimitiveEntities: Entity[] = [];
  useCases: UseCase[];
  presenters: Presenter[];
  views: View[];

  pendingFields: Field[] = [];
  pendingMethods: Method[] = [];
  pendingParams: Field[] = [];

  useCaseName: string;
  presenterName: string;
  viewName: string;

  //for presenter
  pendingUseCases: UseCase[] = [];
  useCaseMethods: Method[] = [];
  pendingPresenters: Presenter[] = [];

  //for view
  presenterMethods: Method[] = [];
  pendingViews: View[] = [];

  fieldsExpanded = false;
  newFieldExpanded = false;
  isConstr = false;
  methodsExpanded = false;
  methodExpanded = false
  newPrameterExpanded = false;
  expandedeMethods: Method[] = [];

  newFlowForm: FormGroup;
  fieldsForm: FormGroup;
  useCaseMethodsForm: FormGroup;
  presenterMethodsForm: FormGroup;
  newMethodFieldsForm: FormGroup;
  methodFieldsForm: FormGroup;
  addUseCaseToFlowForm: FormGroup;
  addPresenterToFlowForm: FormGroup;
  viewMethodsForm: FormGroup;

  @Output() flowEmitter: EventEmitter<Flow> = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private inputData: NewFlowInputData) {

    this.fieldsForm = this.formBuilder.group({
      entity: "",
      parameterizedEntity: "",
      fieldName: ""
    });

    this.methodFieldsForm = this.formBuilder.group({
      entity: "",
      fieldName: ""
    });

    this.newMethodFieldsForm = this.formBuilder.group({
      entity: "",
      fieldName: "",
      parameterizedEntity: ""
    });

    this.useCaseMethodsForm = this.formBuilder.group({
      mehtodName: "",
      parameters: [],
      returnType: "",
      returnParameterizedEntity: ""
    });

    this.addUseCaseToFlowForm = this.formBuilder.group({
      useCase: ""
    });

    this.addPresenterToFlowForm = this.formBuilder.group({
      presenter: ""
    });

    this.presenterMethodsForm = this.formBuilder.group({
      mehtodName: "",
      parameters: [],
      returnType: "",
      returnParameterizedEntity: "",
      useCaseToCall: "",
      useCaseMethodToCall: ""
    });

    this.viewMethodsForm = this.formBuilder.group({
      mehtodName: "",
      parameters: [],
      returnType: "",
      returnParameterizedEntity: "",
      presenterToCall: "",
      presenterMethodToCall: ""
    });

    this.newFlowForm = this.formBuilder.group({
      mainPackageName: "",
      flowName: ""
    });

    this.allEntities = inputData.allEntities;
    this.allEntities.forEach(e => {
      if (!e.typePackage && e.name[0] !== e.name[0].toUpperCase()) {
        return;
      }
      this.nonPrimitiveEntities.push(e);
    });
    this.useCases = inputData.useCases;
    this.presenters = inputData.presenters;
    this.views = inputData.views;

  }

  ngOnInit(): void {
  }

  removeField(index) {
    this.pendingFields.splice(index, 1);
  }

  removeUseCase(index) {
    this.pendingUseCases.splice(index, 1);
  }

  removePresenter(index) {
    this.pendingPresenters.splice(index, 1);
  }

  onFieldSubmit(fieldData: NewField) {
    let field = new Field(fieldData.fieldName, fieldData.entity.name, fieldData.entity.typePackage, this.isConstr);

    if (fieldData.parameterizedEntity) {
      let parameterizedType = new ParameterizedType(fieldData.parameterizedEntity.name,
        fieldData.parameterizedEntity.typePackage);
      field.parameterizedType = parameterizedType;
    }

    this.pendingFields.push(field);

    this.isConstr = false;
    this.isTypedParameterOpened = false;
    this.fieldsForm.reset();
  }

  onTypeSelected(event) {
    this.isTypedParameterOpened = event.value.isParameterized;
  }

  onReturnTypeSelected(event) {
    this.isReturnTypedParameterOpened = event.value.isParameterized;
  }

  onMethodParamTypeSelected(event) {
    this.isMethodParamTypedParameterOpened = event.value.isParameterized;
  }

  expandMethod(method: Method) {
    this.expandedeMethods.push(method);
  }

  clearPendingParams() {
    this.pendingParams = [];
  }

  collapseMethod(method: Method) {
    const index = this.expandedeMethods.indexOf(method, 0);
    if (index > -1) {
      this.expandedeMethods.splice(index, 1);
    }
  }

  switchExpansionForMethod(method: Method) {
    if (this.isMethodExpanded(method)) {
      this.collapseMethod(method);
    } else {
      this.expandMethod(method);
    }
  }

  isMethodExpanded(method: Method): boolean {
    return this.expandedeMethods.indexOf(method) != -1;
  }

  removeMethodParam(methodIndex, paramIndex) {
    this.pendingMethods[methodIndex].parameters.splice(paramIndex, 1);
  }

  // NAVIGATIONS START
  onNewFlowSubmit(newFlow: NewFlowComponent) {
    if (!newFlow.mainPackageName) {
      alert("Invalid main package name")
      return;
    }

    if (!newFlow.flowName) {
      alert("Invalid flow name")
      return;
    }

    this.mainPackageName = newFlow.mainPackageName;
    this.flowName = newFlow.flowName;

    this.resetSelectedTab();
    this.isUseCaseTab = true;
  }

  private resetCommonFields() {
    this.pendingFields = [];
    this.pendingMethods = [];
    this.pendingParams = [];

    this.fieldsExpanded = false;
    this.newFieldExpanded = false;
    this.isConstr = false;
    this.isTypedParameterOpened = false;
    this.isReturnTypedParameterOpened = false;
    this.isMethodParamTypedParameterOpened = false;
    this.methodsExpanded = false;
    this.methodExpanded = false
    this.newPrameterExpanded = false;
    this.expandedeMethods = [];

    this.newMethodFieldsForm.reset();
  }

  goBackFromUseCaseTab() {
    this.resetCommonFields();

    this.pendingUseCases = [];
    this.useCaseName = null;

    this.resetSelectedTab();
    this.isNewFlowTab = true;
  }

  goNextFromUseCaseTab() {
    this.resetCommonFields();

    this.resetSelectedTab();
    this.isPresenterTab = true;
  }

  goBackFromPresenterTab() {
    this.resetCommonFields();

    this.resetSelectedTab();
    this.isUseCaseTab = true;
  }

  goNextFromPresenterTab() {
    this.resetCommonFields();

    this.useCaseMethods = [];

    this.resetSelectedTab();
    this.isViewTab = true;
  }

  goBackFromViewTab() {
    this.resetCommonFields();

    this.pendingPresenters = [];
    this.presenterMethods = [];
    this.pendingViews = [];

    this.resetSelectedTab();
    this.isPresenterTab = true;
  }

  // NAVIGATIONS END

  onNewFieldToMethodSubmit(fieldData: NewField, method: Method) {
    let field = new Field(fieldData.fieldName, fieldData.entity.name, fieldData.entity.typePackage, false);

    if (fieldData.parameterizedEntity) {
      field.parameterizedType = new Field("", fieldData.parameterizedEntity.name, fieldData.parameterizedEntity.typePackage, false);
    }

    method.parameters.push(field);
  }

  onPendingParamSubmit(fieldData: NewField) {
    let field = new Field(fieldData.fieldName, fieldData.entity.name, fieldData.entity.typePackage, false);

    if (fieldData.parameterizedEntity) {
      field.parameterizedType = new Field("", fieldData.parameterizedEntity.name, fieldData.parameterizedEntity.typePackage, false);
    }

    this.pendingParams.push(field);
    this.isMethodParamTypedParameterOpened = false;
    this.isReturnTypedParameterOpened = false;
    this.newMethodFieldsForm.reset();
  }

  removePendingMethodParam(index) {
    this.pendingParams.splice(index, 1);
  }

  onUseCaseMethodSubmit(methodData: NewUseCaseMethod) {
    let newMethod = new Method(methodData.mehtodName, this.pendingParams, false);

    let returnType = new Field("", methodData.returnType.name, methodData.returnType.typePackage, false);
    if (methodData.returnParameterizedEntity) {
      let parameterizedType = new ParameterizedType(methodData.returnParameterizedEntity.name,
        methodData.returnParameterizedEntity.typePackage);

      returnType.parameterizedType = parameterizedType;
    }

    newMethod.returnType = returnType;

    this.isReturnTypedParameterOpened = false;
    this.isMethodParamTypedParameterOpened = false;
    this.pendingMethods.push(newMethod);
    this.pendingParams = [];
    this.useCaseMethodsForm.reset();
  }

  onPresenterMethodSubmit(methodData: NewPresenterMethod) {
    let newMethod = new Method(methodData.mehtodName, this.pendingParams, false);

    let returnType = new Field("", methodData.returnType.name, methodData.returnType.typePackage, false);
    if (methodData.returnParameterizedEntity) {
      let parameterizedType = new ParameterizedType(methodData.returnParameterizedEntity.name,
        methodData.returnParameterizedEntity.typePackage);

      returnType.parameterizedType = parameterizedType;
    }

    newMethod.returnType = returnType;

    newMethod.classToCall = methodData.useCaseToCall;
    newMethod.classMethodToCall = methodData.useCaseMethodToCall;

    this.isReturnTypedParameterOpened = false;
    this.isMethodParamTypedParameterOpened = false;
    this.pendingMethods.push(newMethod);
    this.pendingParams = [];
    this.useCaseMethods = [];
    this.presenterMethodsForm.reset();
  }

  onViewMethodSubmit(methodData: NewViewMethod) {
    let newMethod = new Method(methodData.mehtodName, this.pendingParams, false);

    let returnType = new Field("", methodData.returnType.name, methodData.returnType.typePackage, false);
    if (methodData.returnParameterizedEntity) {
      let parameterizedType = new ParameterizedType(methodData.returnParameterizedEntity.name,
        methodData.returnParameterizedEntity.typePackage);

      returnType.parameterizedType = parameterizedType;
    }

    newMethod.returnType = returnType;

    newMethod.classToCall = methodData.presenterToCall;
    newMethod.classMethodToCall = methodData.presenterMethodToCall;

    this.isReturnTypedParameterOpened = false;
    this.isMethodParamTypedParameterOpened = false;
    this.pendingMethods.push(newMethod);
    this.pendingParams = [];
    this.presenterMethods = [];
    this.viewMethodsForm.reset();
  }

  onAddUseCaseToFlow(newUseCase: NewUseCaseToFlow) {
    this.pendingUseCases.push(newUseCase.useCase);
  }

  onAddPresenterToFlow(newPresenter: NewPresenterToFlow) {
    this.pendingPresenters.push(newPresenter.presenter);
  }

  saveNewUseCase() {
    let useCaseName = this.useCaseName;
    if (!useCaseName || useCaseName === "UseCaseName" || /\s/.test(useCaseName)) {
      alert("Invalid Use Case nane");
    }

    let useCase = this.generateUseCase(useCaseName);
    this.pendingUseCases.push(useCase);
  }

  presenterMethodUseCaseSelected(event) {
    if (event.isUserInput) {
      let selectedUseCase = event.source.value;
      this.useCaseMethods = [...selectedUseCase.methods];
    } else {
      this.useCaseMethods = []
    }
  }

  viewMethodUseCaseSelected(event) {
    if (event.isUserInput) {
      let selectedPresenter = event.source.value;
      this.presenterMethods = [...selectedPresenter.methods];
    } else {
      this.presenterMethods = []
    }
  }

  saveNewPresenter() {
    let presenterName = this.presenterName;
    if (!presenterName || presenterName === "PresenterName" || /\s/.test(presenterName)) {
      alert("Invalid Presenter Name");
    }

    let presenter = this.generatePresenter(presenterName);
    this.pendingPresenters.push(presenter);
  }

  saveNewView() {
    let viewName = this.viewName;
    if (!viewName || viewName === "ViewName" || /\s/.test(viewName)) {
      alert("Invalid View Name");
    }

    let view = this.generateView(viewName);
    this.pendingViews.push(view);
  }

  private generateUseCase(useCaseName: string): UseCase {
    let useCasePackage = this.mainPackageName + ".usecases." + this.deCapitalize(this.flowName);

    let useCaseFields = [...this.pendingFields];

    let useCase = new UseCase(useCaseName, useCasePackage, useCaseFields, [], [], []);

    for (let i = 0; i < this.pendingMethods.length; i++) {
      let method = this.pendingMethods[i];
      let initialMethodParams = [];
      method.parameters.forEach(val => initialMethodParams.push(Object.assign({}, val)));

      let methodReturnType = method.returnType;
      let methodName = method.name;
      let capitalizedMethodName = this.capitalize(methodName);

      //repo  
      let repoName = this.capitalize(methodName) + "Repository";
      let repoPackage = useCasePackage;
      let repoMethodParamList = [];
      initialMethodParams.forEach(val => repoMethodParamList.push(Object.assign({}, val)));

      //use case listener
      let useCaseListenerName = "On" + this.capitalize(method.name) + "Listener";
      let useCaseListener = new Listener(
        useCaseListenerName,
        useCasePackage + "." + useCaseName, []);

      useCase.listeners.push(useCaseListener);

      let useCaseListenerParam = new Field("listener", useCaseListener.name, useCaseListener.typePackage, false);
      method.parameters.push(useCaseListenerParam);

      //repo
      let repoListName = repoName + "Listener";

      let repoSuccesMthField = new Field("response", methodReturnType.type, methodReturnType.typePackage, false);
      repoSuccesMthField.parameterizedType = methodReturnType.parameterizedType;
      let repoListenerSucccessMethod = new Method("on" + capitalizedMethodName + "Success", [repoSuccesMthField], true);
      let repoListenerFailureMethod = new Method("on" + capitalizedMethodName + "Failure",
        [new Field("ex", "Throwable", "", false)], true);
      let repoListener = new Listener(repoListName, repoPackage + "." + repoName,
        [repoListenerSucccessMethod, repoListenerFailureMethod]);
      let repoListenerParam = new Field("listener", repoListener.name, repoListener.typePackage, false);
      repoMethodParamList.push(repoListenerParam);
      let repoMethod = new Method(methodName, repoMethodParamList, true);
      let repository = new Repository(repoName, [repoMethod], [repoListener]);

      //use case listener
      let useCaseListenerSuccesMthField = new Field("response", methodReturnType.type, methodReturnType.typePackage, false);
      useCaseListenerSuccesMthField.parameterizedType = methodReturnType.parameterizedType;
      let succcessMethod = new Method("on" + capitalizedMethodName + "Success", [useCaseListenerSuccesMthField], true);
      let failureMethod = new Method("on" + capitalizedMethodName + "Failure",
        [new Field("ex", "Throwable", "", false)], true);

      useCaseListener.methods.push(succcessMethod);
      useCaseListener.methods.push(failureMethod);

      let useCaseListenerSuccessCallBack = new Callback(
        repoListenerSucccessMethod.name, "listener", succcessMethod.name, succcessMethod.parameters);
      let useCaseListenerFailureCallBack = new Callback(
        repoListenerFailureMethod.name, "listener", failureMethod.name, failureMethod.parameters);

      let repoFieldInUseCase = new Field(this.deCapitalize(repoName), repoName, repoPackage, true);
      useCase.fields.push(repoFieldInUseCase);

      let callBackInfoParams = [];
      initialMethodParams.forEach(val => callBackInfoParams.push(Object.assign({}, val)));

      let callBackInfo = new CallBackInfo(repoFieldInUseCase.name, methodName, repoListener.name, repoListener.typePackage,
        callBackInfoParams, [useCaseListenerSuccessCallBack, useCaseListenerFailureCallBack]);

      method.callbackInfo = callBackInfo;

      useCase.repositories.push(repository);

      useCase.methods.push(method);
    }

    return useCase;
  }


  private generatePresenter(presenterName: string): Presenter {
    let presenterPackage = this.mainPackageName + ".presenters." + this.flowName;

    let presenterFields = [...this.pendingFields];
    let presenter = new Presenter(presenterName, presenterPackage, presenterFields, null, [], []);

    //presenter listener
    let viewName = presenterName + "View";
    let view = new Listener(
      viewName,
      presenterPackage + "." + presenterName, []);

    //lame way to get the plural + s
    let bindingListName = this.deCapitalize(view.name) + "s";
    let bindingList = new BindingList(bindingListName, view.name, view.typePackage);

    presenter.bindingList = bindingList;
    presenter.listeners.push(view);

    for (let i = 0; i < this.pendingMethods.length; i++) {
      let method = this.pendingMethods[i];
      let initialMethodParams = [];
      method.parameters.forEach(val => initialMethodParams.push(Object.assign({}, val)));

      let methodReturnType = method.returnType;
      let methodName = method.name;
      let capitalizedMethodName = this.capitalize(methodName);

      let useCaseToCall = method.classToCall;

      let useCaseFieldInPresenter = new Field(this.deCapitalize(useCaseToCall.name), useCaseToCall.name,
        useCaseToCall.typePackage, true);

      if (!this.fieldsContainField(presenter.fields, useCaseFieldInPresenter)) {
        presenter.fields.push(useCaseFieldInPresenter);
      }

      let presenterListenerSuccesMthField = new Field("response", methodReturnType.type, methodReturnType.typePackage, false);
      presenterListenerSuccesMthField.parameterizedType = methodReturnType.parameterizedType;
      let viewSucccessMethod = new Method("on" + capitalizedMethodName + "Success",
        [presenterListenerSuccesMthField], true);
      let viewFailureMethod = new Method("on" + capitalizedMethodName + "Failure",
        [new Field("ex", "Throwable", "", false)], true);

      view.methods.push(viewSucccessMethod);
      view.methods.push(viewFailureMethod);

      let useCaseMethodToCall = method.classMethodToCall;

      let useCaseSuccessMethodName = useCaseMethodToCall.callbackInfo.callbacks[0].name;
      let viewSuccessCallBack = new Callback(
        useCaseSuccessMethodName, this.deCapitalize(view.name), viewSucccessMethod.name, viewSucccessMethod.parameters);
      viewSuccessCallBack.bindingList = bindingList;

      let useCaseFailureMethodName = useCaseMethodToCall.callbackInfo.callbacks[1].name;
      let viewFailureCallBack = new Callback(
        useCaseFailureMethodName, this.deCapitalize(view.name), viewFailureMethod.name, viewFailureMethod.parameters);
      viewFailureCallBack.bindingList = bindingList;

      let params = useCaseMethodToCall.parameters.slice(0, useCaseMethodToCall.parameters.length - 1);


      let listenerParamNameInUseCaseMethod = useCaseMethodToCall.callbackInfo.callbacks[0].listenerName;
      let useCaseListenerNameToImplement: string;
      let useCaseListenerPackageToImplement: string;
      for (let k = 0; k < useCaseMethodToCall.parameters.length; k++) {
        if (listenerParamNameInUseCaseMethod === useCaseMethodToCall.parameters[k].name) {
          useCaseListenerNameToImplement = useCaseMethodToCall.parameters[k].type;
          useCaseListenerPackageToImplement = useCaseMethodToCall.parameters[k].typePackage;
        }
      }

      let callBackInfo = new CallBackInfo(this.deCapitalize(useCaseToCall.name),
        useCaseMethodToCall.name, useCaseListenerNameToImplement, useCaseListenerPackageToImplement,
        params, [viewSuccessCallBack, viewFailureCallBack]);

      method.callbackInfo = callBackInfo;

      presenter.methods.push(method);
    }

    return presenter;
  }

  generateView(viewName: string): View {
    let viewPackage = this.mainPackageName + ".views." + this.flowName;

    let viewFields = [...this.pendingFields];
    let view = new View(viewName, viewPackage, viewFields, [], []);

    for (let i = 0; i < this.pendingMethods.length; i++) {
      let method = this.pendingMethods[i];
      let initialMethodParams = [];
      method.parameters.forEach(val => initialMethodParams.push(Object.assign({}, val)));

      let presenterToCall = method.classToCall;

      let viewInterfaces = [];

      presenterToCall.listeners.forEach(viewInt => {
        let mths: Method[] = [];

        viewInt.methods.forEach(m => {
          let newM = { ...m };
          newM.isAbstract = false;
          newM.override = true;
          mths.push(newM);
        });

        viewInterfaces.push(new Interface(viewInt.name, viewInt.typePackage, mths));
      });

      viewInterfaces.forEach(viewInt => {
        for (let j = 0; j < view.interfaces.length; j++) {
          if (view.interfaces[j].name === viewInt.name) {
            return;
          }
        }
        view.interfaces.push(viewInt);
      });

      let presenterFieldInView = new Field(this.deCapitalize(presenterToCall.name),
        presenterToCall.name, presenterToCall.typePackage, true);
      if (!this.fieldsContainField(view.fields, presenterFieldInView)) {
        view.fields.push(presenterFieldInView);
      }

      let presenterMethodToCall = method.classMethodToCall;

      let parameters: string[] = presenterMethodToCall.parameters.map(param => { return param.name });

      let callCode = new CallCode(presenterFieldInView.name, presenterMethodToCall.name, parameters);

      method.callCode = callCode;

      view.methods.push(method);
    }

    return view;
  }

  private fieldsContainField(fields: Field[], fieldToCheck: Field): boolean {
    for (let i = 0; i < fields.length; i++) {
      if (fieldToCheck.name === fields[i].name) {
        return true;
      }
    }
    return false;
  }


  private capitalize(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  private deCapitalize(text: string): string {
    return text.charAt(0).toLowerCase() + text.slice(1);
  }

  private resetSelectedTab() {
    this.isNewFlowTab = false;
    this.isUseCaseTab = false;
    this.isPresenterTab = false;
    this.isViewTab = false;
  }

  finish() {
    let flow = new Flow(this.flowName, this.pendingUseCases, this.pendingPresenters, this.pendingViews);
    this.flowEmitter.emit(flow);
  }
}

