import { Entity } from './entity';
import { Listener } from '../software-element-parts/listener';
import { Method } from '../software-element-parts/method';
import { Field } from '../software-element-parts/field';
import { BindingList } from '../software-element-parts/binding-list';
import { SoftwareElementAction } from '../software-elements-holders/software-element-action';

export class Presenter extends Entity {
    bindingList: BindingList;
    methods: Method[];
    listeners: Listener[];
    action: SoftwareElementAction;

    constructor(name: string, typePackage: string, fields: Field[],
        bindingList: BindingList, methods: Method[], listeners: Listener[]) {

        super(name, typePackage, fields);
        this.bindingList = bindingList;
        this.methods = methods;
        this.listeners = listeners;
    }
}
