import { Injectable } from '@angular/core';
import { Entity } from '../shared/software-elements/entity';
import * as builder from 'xmlbuilder2';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';
import { Field } from '../shared/software-element-parts/field';
import { BindingList } from '../shared/software-element-parts/binding-list';
import { Method } from '../shared/software-element-parts/method';
import { CallCode } from '../shared/software-element-parts/call-code';
import { Listener } from '../shared/software-element-parts/listener';
import { Repository } from '../shared/software-element-parts/repository';
import { UseCase } from '../shared/software-elements/use-case';
import { Presenter } from '../shared/software-elements/presenter';
import { View } from '../shared/software-elements/view';
import { Interface } from '../shared/software-element-parts/interface';

@Injectable({
    providedIn: 'root'
})
export class XMLBuilderService {


    constructor() {

    }

    public createXML(entities: Entity[], useCases: UseCase[], presenters: Presenter[], views: View[]): string {

        let program: XMLBuilder = builder.create().ele("program");

        this.addEntitiesToXml(entities, program);
        this.addUseCasesToXml(useCases, program);
        this.addPresentersToXml(presenters, program);
        this.addViewsToXml(views, program);

        const xml = program.end({ prettyPrint: true });

        return xml;
    }

    private addEntitiesToXml(entities: Entity[], program: XMLBuilder) {
        let entitiesNode: XMLBuilder = program.ele("entities");

        if (!entities || entities.length <= 0) return;

        entities.forEach(entity => {
            if (!entity) return;

            let entityNode: XMLBuilder = entitiesNode.ele("entity");
            entityNode.att("name", entity.name);
            entityNode.att("package", entity.typePackage);

            let fieldsNode: XMLBuilder = entityNode.ele("fields");
            this.addFields(entity.fields, fieldsNode);
        });
    }

    private addUseCasesToXml(useCases: UseCase[], program: XMLBuilder) {
        let useCasesNode: XMLBuilder = program.ele("useCases");

        if (!useCases || useCases.length <= 0) return;

        useCases.forEach(useCase => {
            if (!useCase) return;

            let useCaseNode: XMLBuilder = useCasesNode.ele("useCase");
            useCaseNode.att("name", useCase.name);
            useCaseNode.att("package", useCase.typePackage);

            let fieldsNode: XMLBuilder = useCaseNode.ele("fields");
            this.addFields(useCase.fields, fieldsNode);

            let methodsNode: XMLBuilder = useCaseNode.ele("methods");
            this.addMethods(useCase.methods, methodsNode);

            let listenersNode: XMLBuilder = useCaseNode.ele("listeners");
            this.addListeners(useCase.listeners, listenersNode);

            let repositoriesNode: XMLBuilder = useCaseNode.ele("repositories");
            this.addRepositories(useCase.repositories, repositoriesNode);
        });
    }

    private addPresentersToXml(presenters: Presenter[], program: XMLBuilder) {
        let presentersNode: XMLBuilder = program.ele("presenters");

        if (!presenters || presenters.length <= 0) return;

        presenters.forEach(presenter => {
            if (!presenter) return;

            let presenterNode: XMLBuilder = presentersNode.ele("presenter");
            presenterNode.att("name", presenter.name);
            presenterNode.att("package", presenter.typePackage);

            let fieldsNode: XMLBuilder = presenterNode.ele("fields");
            this.addFields(presenter.fields, fieldsNode);
            this.addBindingList(presenter.bindingList, fieldsNode);

            let methodsNode: XMLBuilder = presenterNode.ele("methods");
            this.addMethods(presenter.methods, methodsNode);

            let listenersNode: XMLBuilder = presenterNode.ele("listeners");
            this.addListeners(presenter.listeners, listenersNode);
        });
    }

    private addViewsToXml(views: View[], program: XMLBuilder) {
        let viewsNode: XMLBuilder = program.ele("views");

        if (!views || views.length <= 0) return;

        views.forEach(view => {
            if (!view) return;

            let viewNode: XMLBuilder = viewsNode.ele("view");
            viewNode.att("name", view.name);
            viewNode.att("package", view.typePackage);

            let fieldsNode: XMLBuilder = viewNode.ele("fields");
            this.addFields(view.fields, fieldsNode);

            let interfacesNode: XMLBuilder = viewNode.ele("interfaces");
            this.addInterfaces(view.interfaces, interfacesNode);

            let methodsNode: XMLBuilder = viewNode.ele("methods");
            this.addMethods(view.methods, methodsNode);
        });
    }

    private addFields(fields: Field[], fieldsNode: XMLBuilder) {
        if (!fields || fields.length <= 0) return;

        fields.forEach(field => {
            if (!field) return;

            let fieldNode: XMLBuilder = fieldsNode.ele("field");
            fieldNode.att("name", field.name);
            fieldNode.att("type", field.type);
            fieldNode.att("typePackage", field.typePackage);

            if (field.parameterizedType) {
                let parameterizedTypeNode = fieldNode.ele("parameterizedType");
                parameterizedTypeNode.att("type", field.parameterizedType.type);
                parameterizedTypeNode.att("typePackage", field.parameterizedType.typePackage);
            }
            fieldNode.att("constructor", field.constr + "");
        });
    }

    private addBindingList(bindingList: BindingList, fieldsNode: XMLBuilder) {
        if (!bindingList) return;

        let bindingListNode: XMLBuilder = fieldsNode.ele("bindingList");
        bindingListNode.att("name", bindingList.name);
        bindingListNode.att("parametrizedType", bindingList.parametrizedType);
        bindingListNode.att("parametrizedTypePackage", bindingList.parametrizedTypePackage);
    }

    private addMethods(methods: Method[], methodsNode: XMLBuilder) {
        if (!methods || methods.length <= 0) return;

        methods.forEach(method => {
            if (!method) return;

            let methodNode: XMLBuilder = methodsNode.ele("method");
            methodNode.att("name", method.name);
            methodNode.att("abstract", method.isAbstract + "");
            methodNode.att("override", method.override + "");

            this.addParameters(method.parameters, methodNode);

            this.addCallBackInfo(method, methodNode);

            this.addCallCode(method.callCode, methodNode);
        });
    }

    private addParameters(parameters: Field[], parent: XMLBuilder) {
        if (!parameters || parameters.length <= 0) return;

        parameters.forEach(param => {
            if (!param) return;

            let paramNode: XMLBuilder = parent.ele("parameter");
            paramNode.att("name", param.name);
            paramNode.att("type", param.type);
            paramNode.att("typePackage", param.typePackage);

            console.log(JSON.stringify(param));

            if (param.parameterizedType) {
                let parameterizedTypeNode = paramNode.ele("parameterizedType");
                parameterizedTypeNode.att("type", param.parameterizedType.type);
                parameterizedTypeNode.att("typePackage", param.parameterizedType.typePackage);
            }
        });
    }

    private addCallBackInfo(method: Method, methodNode: XMLBuilder) {
        let callBackInfo = method.callbackInfo;

        if (!callBackInfo) return;

        let callBackInfoNode: XMLBuilder = methodNode.ele("callBackInfo");

        callBackInfoNode.att("callBackName", callBackInfo.callBackName);
        callBackInfoNode.att("callBackMethodName", callBackInfo.callBackMethodName);
        callBackInfoNode.att("callBackClass", callBackInfo.callBackClass);
        callBackInfoNode.att("callBackClassPackage", callBackInfo.callBackClassPackage);

        this.addParameters(callBackInfo.parameters, callBackInfoNode);

        let callBackInfoCallBacks = callBackInfo.callbacks;

        if (!callBackInfoCallBacks || callBackInfoCallBacks.length <= 0) return;

        callBackInfoCallBacks.forEach(callback => {
            if (!callback) return;

            let callBackNode: XMLBuilder = callBackInfoNode.ele("callBack");
            callBackNode.att("name", callback.name);
            callBackNode.att("listenerName", callback.listenerName);
            callBackNode.att("listenerMethodName", callback.listenerMethodName);
            this.addParameters(callback.parameters, callBackNode);

            if (callback.bindingList) {
                this.addBindingList(callback.bindingList, callBackNode);
            }
        });
    }

    private f() {

    }

    private addCallCode(callCode: CallCode, methodNode: XMLBuilder) {
        if (!callCode) return;

        let callCodeNode: XMLBuilder = methodNode.ele("callCode");
        callCodeNode.att("objectToCall", callCode.objectToCall);
        callCodeNode.att("methodToCall", callCode.methodToCall);

        let callParameters = callCode.parameters;
        if (!callParameters || callParameters.length <= 0) return;

        callParameters.forEach(callParam => {
            if (!callParam) return;

            let callParamNode: XMLBuilder = callCodeNode.ele("callParameter");
            callParamNode.att("name", callParam);
        });

    }

    private addListeners(listeners: Listener[], listenersNode: XMLBuilder) {
        if (!listeners || listeners.length <= 0) return;

        listeners.forEach(listener => {
            if (!listener) return;

            let listenerNode: XMLBuilder = listenersNode.ele("listener");
            listenerNode.att("name", listener.name);

            let methodsNode: XMLBuilder = listenerNode.ele("methods");
            this.addMethods(listener.methods, methodsNode);
        });
    }

    private addRepositories(repositories: Repository[], repositoriesNode: XMLBuilder) {
        if (!repositories || repositories.length <= 0) return;

        repositories.forEach(repository => {
            if (!repository) return;

            let repositoryNode: XMLBuilder = repositoriesNode.ele("repository");
            repositoryNode.att("name", repository.name);

            let methodsNode: XMLBuilder = repositoryNode.ele("methods");
            this.addMethods(repository.methods, methodsNode);

            let listenersNode: XMLBuilder = repositoryNode.ele("listeners");
            this.addListeners(repository.listeners, listenersNode);
        });
    }

    private addInterfaces(interfaces: Interface[], interfacesNode: XMLBuilder) {
        if (!interfaces || interfaces.length <= 0) return;

        interfaces.forEach(interf => {
            if (!interf) return;

            let interfaceNode: XMLBuilder = interfacesNode.ele("interface");
            interfaceNode.att("name", interf.name);
            interfaceNode.att("package", interf.typePackage);

            this.addMethods(interf.methods, interfaceNode);
        });
    }
}
