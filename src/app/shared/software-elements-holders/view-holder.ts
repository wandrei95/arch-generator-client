import { SoftwareElementHolder } from './software-element-holder';
import { Field } from '../software-element-parts/field';
import { Method } from '../software-element-parts/method';
import { View } from '../software-elements/view';
import { SoftwareElementAction } from './software-element-action';
import { Layer } from '../software-element-parts/layer';

export class ViewHolder extends SoftwareElementHolder {

    view: View

    constructor(view: View, mainPackageName: string, softwareElementAction: SoftwareElementAction) {
        super(mainPackageName, softwareElementAction);
        this.view = view;
    }

    getName(): string {
        if (this.view) {
            return this.view.name;
        }
        return "";
    }

    getFields(): Field[] {
        if (this.view) {
            return this.view.fields;
        }
        return [];
    }

    getMethods(): Method[] {
        if (this.view) {
            return this.view.methods;
        }
        return [];
    }

    allowMethods(): boolean {
        return true;
    }

    getLayer(): Layer {
        return Layer.VIEW
    }

}