import { Field } from '../software-element-parts/field';
import { SoftwareElementAction } from '../software-elements-holders/software-element-action';

export class Entity {
    name: string;
    typePackage: string;
    fields: Field[];
    action: SoftwareElementAction;
    isParameterized: boolean;

    constructor(name: string, typePackage: string, fields: Field[]) {
        
        this.name = name;
        this.typePackage = typePackage;
        this.fields = fields;
    }
}
