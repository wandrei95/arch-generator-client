import { ParameterizedType } from './type';

export class Field {
    name: string;
    type: string;
    typePackage: string;
    constr: boolean;
    parameterizedType: ParameterizedType

    constructor(name: string, type: string, typePackage: string, constr: boolean) {
       
        this.name = name;
        this.type = type;
        this.typePackage = typePackage;
        this.constr = constr;
    }
}
