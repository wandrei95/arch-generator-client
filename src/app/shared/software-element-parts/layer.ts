export enum Layer {
    ENTITY,
    USE_CASE,
    PRESENTER,
    VIEW,
}