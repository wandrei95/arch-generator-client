import { Field } from './field';
import { Callback } from './callback';

export class CallBackInfo {
    callBackName: string;
    callBackMethodName: string;
    callBackClass: string;
    callBackClassPackage: string;
    parameters: Field[];
    callbacks: Callback[];

    constructor(callBackName: string, callBackMethodName: string, callBackClass: string,
        callBackClassPackage: string, parameters: Field[], callbacks: Callback[]) {

        this.callBackName = callBackName;
        this.callBackMethodName = callBackMethodName;
        this.callBackClass = callBackClass;
        this.callBackClassPackage = callBackClassPackage;
        this.parameters = parameters;
        this.callbacks = callbacks;
    }
}
