import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SoftwareElementHolder } from 'src/app/shared/software-elements-holders/software-element-holder';
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';

import { Field } from 'src/app/shared/software-element-parts/field';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Entity } from 'src/app/shared/software-elements/entity';
import { DefaultEntitiesService } from 'src/app/io/default-entities.ervice';
import { Layer } from 'src/app/shared/software-element-parts/layer';
import { ParameterizedType } from '../shared/software-element-parts/type';

interface NewField {
  entity: Entity;
  parameterizedEntity: Entity;
  fieldName: string;
}

@Component({
  selector: 'app-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss']
})
export class EntitiesComponent implements OnInit {

  @Output() entityEmitter: EventEmitter<Entity> = new EventEmitter();


  fieldsExpanded = false;
  newsFieldExpanded = false;
  isTypedParameterOpened = false;
  fieldsForm: FormGroup;

  allEntities: Entity[] = [];
  nonPrimitiveEntities: Entity[] = [];

  fields: Field[] = [];

  title: string;
  mainPackage: string;
  isConstr: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private defaultEntitiesService: DefaultEntitiesService,
    @Inject(MAT_DIALOG_DATA) public softwareElemHolder: SoftwareElementHolder) {

    this.fieldsForm = this.formBuilder.group({
      entity: "",
      parameterizedEntity: "",
      fieldName: ""
    });

    defaultEntitiesService.getDefaultentities().forEach(defaultEntity => {
      this.allEntities.push(defaultEntity);
    });

    if (softwareElemHolder.allEntities) {
      softwareElemHolder.allEntities.forEach(val => this.allEntities.push(Object.assign({}, val)));
    }

    if (softwareElemHolder.getFields()) {
      softwareElemHolder.getFields().forEach(val => this.fields.push(Object.assign({}, val)));
    }

    this.allEntities.forEach(e => {
      if (!e.typePackage && e.name[0] !== e.name[0].toUpperCase()) {
        return;
      }
      this.nonPrimitiveEntities.push(e);
    });

    this.title = softwareElemHolder.getName();
    this.mainPackage = softwareElemHolder.mainPackageName;
  }

  ngOnInit() {
  }

  getTitle(): string {
    return this.title;
  }

  getFields(): Field[] {
    return this.fields;
  }

  hasMethods(): boolean {
    return this.softwareElemHolder.allowMethods();
  }

  onFieldSubmit(fieldData: NewField) {
    let field = new Field(fieldData.fieldName, fieldData.entity.name, fieldData.entity.typePackage, this.isConstr);

    if (fieldData.parameterizedEntity) {
      let parameterizedType = new ParameterizedType(fieldData.parameterizedEntity.name,
        fieldData.parameterizedEntity.typePackage);
      field.parameterizedType = parameterizedType;
    }

    this.fields.push(field);

    this.isTypedParameterOpened = false;
    this.fieldsForm.reset();
  }

  removeField(index) {
    this.fields.splice(index, 1);
  }

  onTypeSelected(event) {
    this.isTypedParameterOpened = event.value.isParameterized;
  }

  save() {
    switch (this.softwareElemHolder.getLayer()) {
      case Layer.ENTITY:

        let pck = this.softwareElemHolder.mainPackageName
          ? this.softwareElemHolder.mainPackageName
          : this.mainPackage + ".entities";

        let entity = new Entity(this.title, pck, this.fields);
        entity.action = this.softwareElemHolder.softwareElementAction;
        this.entityEmitter.emit(entity);
        break;
      case Layer.USE_CASE:
        break;
      case Layer.PRESENTER:
        break;
      case Layer.VIEW:
        break;
    }
  }
}
