import { Field } from './field';
import { CallCode } from './call-code';
import { CallBackInfo } from './callback-info';
import { Entity } from '../software-elements/entity';
import { UseCase } from '../software-elements/use-case';

export class Method {
    name: string;
    parameters: Field[];
    isAbstract: boolean;
    override: boolean;
    callCode: CallCode;
    callbackInfo: CallBackInfo;
    returnType: Field;
    classToCall: UseCase;
    classMethodToCall: Method;

    constructor(name: string, parameters: Field[], isAbstract: boolean) {

        this.name = name;
        this.parameters = parameters;
        this.isAbstract = isAbstract;
    }
}
