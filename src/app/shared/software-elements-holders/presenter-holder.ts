import { SoftwareElementHolder } from './software-element-holder';
import { Field } from '../software-element-parts/field';
import { Method } from '../software-element-parts/method';
import { Presenter } from '../software-elements/presenter';
import { SoftwareElementAction } from './software-element-action';
import { Layer } from '../software-element-parts/layer';

export class PresenterHolder extends SoftwareElementHolder {

    presenter: Presenter

    constructor(presenter: Presenter, mainPackageName: string, softwareElementAction: SoftwareElementAction) {
        super(mainPackageName, softwareElementAction);
        this.presenter = presenter;
    }

    getName(): string {
        if (this.presenter) {
            return this.presenter.name;
        }
        return "";
    }
    
    getFields(): Field[] {
        if (this.presenter) {
            return this.presenter.fields;
        }
        return [];
    }

    getMethods(): Method[] {
        if (this.presenter) {
            return this.presenter.methods;
        }
        return [];
    }

    allowMethods(): boolean {
        return true;
    }

    getLayer(): Layer {
        return Layer.PRESENTER
    }

}