import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftwareLayersComponent } from './software-layers.component';

describe('SoftwareLayersComponent', () => {
  let component: SoftwareLayersComponent;
  let fixture: ComponentFixture<SoftwareLayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoftwareLayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareLayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
