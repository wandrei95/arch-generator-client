import { UseCase } from '../software-elements/use-case';
import { Entity } from '../software-elements/entity';
import { Presenter } from '../software-elements/presenter';
import { View } from '../software-elements/view';

export class NewFlowInputData {
    allEntities: Entity[];
    useCases: UseCase[];
    presenters: Presenter[];
    views: View[];

    constructor(allEntities: Entity[], useCases: UseCase[], presenters: Presenter[], views: View[]) {
        this.allEntities = [...allEntities];
        this.useCases = [...useCases];
        this.presenters = [...presenters];
        this.views = [...views];
    }
}