import { Field } from './field';
import { BindingList } from './binding-list';

export class Callback {
    name: string;
    listenerName: string;
    listenerMethodName: string;
    parameters: Field[];
    bindingList: BindingList;

    constructor(name: string, listenerName: string, listenerMethodName: string, parameters: Field[]) {

        this.name = name;
        this.listenerName = listenerName;
        this.listenerMethodName = listenerMethodName;
        this.parameters = parameters;
    }
}
