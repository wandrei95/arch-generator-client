export class BindingList {
    name: string;
    parametrizedType: string;
    parametrizedTypePackage: string;

    constructor(name: string, parametrizedType: string, parametrizedTypePackage: string) {
        this.name = name;
        this.parametrizedType = parametrizedType;
        this.parametrizedTypePackage = parametrizedTypePackage;
    }
}
