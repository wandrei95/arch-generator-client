import { Method } from './method';

export class Interface {
    name: string;
    typePackage: string;
    methods: Method[];

    constructor(name: string, typePackage: string, methods: Method[]) {
  
        this.name = name;
        this.typePackage = typePackage;
        this.methods = methods;
    }
}
