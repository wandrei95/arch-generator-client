import { UseCase } from '../software-elements/use-case';
import { Presenter } from '../software-elements/presenter';
import { View } from '../software-elements/view';

export class Flow {
    name: string;
    usecases: UseCase[];
    presenters: Presenter[];
    views: View[];

    constructor(name: string, usecases: UseCase[], presenters: Presenter[], views: View[]) {
        this.name = name;
        this.usecases = usecases;
        this.presenters = presenters;
        this.views = views;
    }
}