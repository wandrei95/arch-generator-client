import { SoftwareElementHolder } from './software-element-holder';
import { Entity } from '../software-elements/entity';
import { Field } from '../software-element-parts/field';
import { Method } from '../software-element-parts/method';
import { SoftwareElementAction } from './software-element-action';
import { Layer } from '../software-element-parts/layer';

export class EntityHolder extends SoftwareElementHolder {

    entity: Entity

    constructor(entity: Entity, mainPackageName: string, softwareElementAction: SoftwareElementAction) {
        super(mainPackageName, softwareElementAction);
        this.entity = entity;
    }

    getName(): string {
        if (this.entity) {
            return this.entity.name;
        }
        return "";
    }

    getFields(): Field[] {
        if (this.entity) {
            return this.entity.fields;
        }
        return [];
    }

    getMethods(): Method[] {
        return [];
    }

    allowMethods(): boolean {
        return false;
    }

    getLayer(): Layer {
        return Layer.ENTITY
    }

}