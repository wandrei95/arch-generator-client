import { Entity } from './entity';
import { Method } from '../software-element-parts/method';
import { Field } from '../software-element-parts/field';
import { Interface } from '../software-element-parts/interface';
import { SoftwareElementAction } from '../software-elements-holders/software-element-action';

export class View extends Entity {
    methods: Method[];
    interfaces: Interface[];
    action: SoftwareElementAction;

    constructor(name: string, typePackage: string, fields: Field[], methods: Method[], interfaces: Interface[]) {

        super(name, typePackage, fields);
        this.methods = methods;
        this.interfaces = interfaces;
    }
}
