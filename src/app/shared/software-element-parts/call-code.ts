export class CallCode {
    objectToCall: string;
    methodToCall: string;
    parameters: string[];

    constructor(objectToCall: string, methodToCall: string, parameters: string[]) {
        this.objectToCall = objectToCall;
        this.methodToCall = methodToCall;
        this.parameters = parameters;
    }
}
