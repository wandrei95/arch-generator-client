import { Injectable } from '@angular/core';
import { Entity } from '../shared/software-elements/entity';

@Injectable({
    providedIn: 'root'
})
export class DefaultEntitiesService {

    private defaultEntities: Entity[];

    constructor() {
        let listOfType = new Entity("List", "java.util", []);
        listOfType.isParameterized = true;

        this.defaultEntities = [
            new Entity("byte", "", []),
            new Entity("Byte", "", []),
            new Entity("short", "", []),
            new Entity("Short", "", []),
            new Entity("int", "", []),
            new Entity("Integer", "", []),
            new Entity("long", "", []),
            new Entity("Long", "", []),
            new Entity("float", "", []),
            new Entity("Float", "", []),
            new Entity("double", "", []),
            new Entity("Double", "", []),
            new Entity("boolean", "", []),
            new Entity("Boolean", "", []),
            new Entity("char", "", []),
            new Entity("Character", "", []),
            new Entity("String", "", []),
            new Entity("Date", "java.util", []),
            listOfType
        ];
    }
    public getDefaultentities(): Entity[] {
        return this.defaultEntities;
    }
}
