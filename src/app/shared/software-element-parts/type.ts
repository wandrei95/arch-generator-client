export class ParameterizedType {
    type: string;
    typePackage: string;

    constructor(type: string, typePackage: string) {
       
        this.type = type;
        this.typePackage = typePackage;
    }
}