import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})
export class ArchService {
    private THE_URL = "http://127.0.0.1:1234/build";

    constructor(private httpClient: HttpClient, private sanitizer: DomSanitizer) {

    }
    
    doTheParsing(value: string) {
        var ab = new ArrayBuffer(value.length);
        var ia = new Uint8Array(ab);

        for (var i = 0; i < value.length; i++) {
            ia[i] = value.charCodeAt(i);
        }
        let blb: Blob = new Blob([ab], { type: 'xml' });

        const formData = new FormData();
        formData.append('file', blb, 'file');

        const requestOptions: Object = {
            responseType: "arraybuffer"
        }

        this.httpClient.post<string>(this.THE_URL, formData, requestOptions).subscribe({
            next: shiet => {
                let blob = new Blob([shiet], {
                    type: 'application/zip'
                });
                var url = window.URL.createObjectURL(blob);

                var a: any = document.createElement("a");
                a.href = url;
                a.download = "code.zip";
                a.click();
                window.URL.revokeObjectURL(url);
            },
            error: err => {
                console.log("Error " + JSON.stringify(err));
            }
        })
    }
}