import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Field } from 'src/app/shared/software-element-parts/field';
import { Method } from 'src/app/shared/software-element-parts/method';
import { UseCase } from 'src/app/shared/software-elements/use-case';
import { Presenter } from 'src/app/shared/software-elements/presenter';

@Component({
  selector: 'dfx-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() field: Field;
  @Input() isParam: boolean;
  @Input() useCase: UseCase;
  @Input() presenter: Presenter;

  @Output() removeFieldItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeParamItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeUseCaseItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() removePresenterItem: EventEmitter<any> = new EventEmitter<any>();

  constructor() {

  }

  ngOnInit() {
    
  }

  removeField() {
    this.removeFieldItem.emit(true);
  }

  removeMethodParam() {
    this.removeParamItem.emit(true);
  }

  removeUseCase() {
    this.removeUseCaseItem.emit(true);
  }

  removePresenter() {
    this.removePresenterItem.emit(true);
  }

}
