import { SoftwareElementHolder } from './software-element-holder';
import { Field } from '../software-element-parts/field';
import { Method } from '../software-element-parts/method';
import { UseCase } from '../software-elements/use-case';
import { SoftwareElementAction } from './software-element-action';
import { Layer } from '../software-element-parts/layer';

export class UseCaseHolder extends SoftwareElementHolder {

    useCase: UseCase

    constructor(useCase: UseCase, mainPackageName: string, softwareElementAction: SoftwareElementAction) {
        super(mainPackageName, softwareElementAction);
        this.useCase = useCase;
    }

    getName(): string {
        if (this.useCase) {
            return this.useCase.name;
        }
        return "";
    }

    getFields(): Field[] {
        if (this.useCase) {
            return this.useCase.fields;
        }
        return [];
    }

    getMethods(): Method[] {
        if (this.useCase) {
            return this.useCase.methods;
        }
        return [];
    }

    allowMethods(): boolean {
        return true;
    }

    getLayer(): Layer {
        return Layer.USE_CASE
    }

}